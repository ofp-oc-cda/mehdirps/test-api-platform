<?php

namespace App\Entity;

use App\Repository\MovieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Controller\RandomMovieController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    collectionOperations: [
        'get' => [
            'normalization_context' => ['groups' => ['collection']]
        ],
        'post',
        'random' => [
            'controller' => RandomMovieController::class,
            'path' => '/movies/random' ,
            'output' => Movie::class,
            'method' => Request::METHOD_GET,
            "pagination_enabled" => false,
            'normalization_context' => ['groups' => ['item']]
        ]
    ],
    itemOperations: [
        'get' => [
            'normalization_context' => ['groups' => ['item']]
        ],
        'put',
        'delete'
    ],
)]
// #[ApiFilter(SearchFilter::class, properties:[
//     'title' => SearchFilter::STRATEGY_PARTIAL,
//     'production_year' => SearchFilter::STRATEGY_PARTIAL,
//     'genre' => SearchFilter::STRATEGY_PARTIAL,
//     'actors' => SearchFilter::STRATEGY_PARTIAL,
//     'directors' => SearchFilter::STRATEGY_PARTIAL,
// ])]
#[ORM\Entity(repositoryClass: MovieRepository::class)]
class Movie
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['collection', 'item'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['collection', 'item'])]
    private ?string $title = null;

    #[ORM\Column]
    #[Groups(['collection', 'item'])]
    private ?int $durable = null;

    #[ORM\Column(length: 255)]
    #[Groups(['collection', 'item'])]
    private ?string $production_year = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['collection', 'item'])]
    private ?string $synopsis = null;

    #[ORM\ManyToOne]
    #[Groups(['collection', 'item'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Genre $genre = null;

    #[ORM\ManyToMany(targetEntity: Person::class)]
    #[ORM\JoinTable(name: 'movie_actors')]
    #[ApiSubresource()]
    #[Groups('item')]
    private Collection $actors;

    #[ORM\ManyToMany(targetEntity: Person::class)]
    #[ORM\JoinTable(name: 'movie_directors')]
    #[ApiSubresource()]
    #[Groups('item')]
    private Collection $directors;

    public function __construct()
    {
        $this->actors = new ArrayCollection();
        $this->directors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDurable(): ?int
    {
        return $this->durable;
    }

    public function setDurable(int $durable): self
    {
        $this->durable = $durable;

        return $this;
    }

    public function getProductionYear(): ?string
    {
        return $this->production_year;
    }

    public function setProductionYear(string $production_year): self
    {
        $this->production_year = $production_year;

        return $this;
    }

    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    public function setSynopsis(string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    public function getGenre(): ?Genre
    {
        return $this->genre;
    }

    public function setGenre(?Genre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection<int, Person>
     */
    public function getActors(): Collection
    {
        return $this->actors;
    }

    public function addActor(Person $actor): self
    {
        if (!$this->actors->contains($actor)) {
            $this->actors->add($actor);
        }

        return $this;
    }

    public function removeActor(Person $actor): self
    {
        $this->actors->removeElement($actor);

        return $this;
    }

    /**
     * @return Collection<int, Person>
     */
    public function getDirectors(): Collection
    {
        return $this->directors;
    }

    public function addDirector(Person $director): self
    {
        if (!$this->directors->contains($director)) {
            $this->directors->add($director);
        }

        return $this;
    }

    public function removeDirector(Person $director): self
    {
        $this->directors->removeElement($director);

        return $this;
    }
}
