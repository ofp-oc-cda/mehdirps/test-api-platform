<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    itemOperations: [
        'get', 'delete', 'put'
    ],
    collectionOperations: [
        'get', 'post'
    ],
    attributes: [
        "pagination_enabled" => false
    ],
)]
#[ApiFilter(SearchFilter::class, properties: [
    'first_name' => SearchFilter::STRATEGY_PARTIAL,
    'last_name' => SearchFilter::STRATEGY_PARTIAL,
])]
#[ApiFilter(OrderFilter::class, properties: ['id' => 'ASC', 'first_name' => 'DESC'])]
#[ORM\Entity(repositoryClass: PersonRepository::class)]
class Person
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('item')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[NotBlank()]
    #[Groups('item')]
    private ?string $first_name = null;

    #[ORM\Column(length: 255)]
    #[NotBlank()]
    #[NotEqualTo(propertyPath: 'first_name')]
    #[Groups('item')]
    private ?string $last_name = null;

    public function __construct()
    {
        $this->movies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }
}
