<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Repository\MovieRepository;

class RandomMovieController
{
    public function __construct(private MovieRepository $movieRepository)
    {
    }
    
    public function __invoke(): Movie
    {
        return $this->movieRepository->getRandomMovie();
    }
}
